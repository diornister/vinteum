<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Auth::routes();

Route::get('/', 'HouseController@index')->name('houseList');
Route::get('editHouse', 'HouseController@editIndex')->name('editHouse');

Route::resource('house', 'HouseController');
Route::get('houseCreate', 'HouseController@create')->name('houseCreate')->middleware('auth');
Route::post('addHouse', 'HouseController@store')->middleware('auth');
Route::post('searchHouse', 'HouseController@search')->name('searchHouse');
Route::post('consumeXML', 'HouseController@consumeXML')->middleware('auth');


Route::get('/viewXML', function(){
	return view('house.xml');
})->middleware('auth');
