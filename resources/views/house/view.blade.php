@extends('layouts.app')
    
<?php
    use illuminate\pagination;
?>

@section('content')    
    <div class="container">
        <div class="d-flex" style="flex-direction: column;">
            <div class="d-flex" style="flex-direction: row; justify-content: center;margin-bottom: 20px">
                <form class="d-flex" action="/searchHouse" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input id="search" class="form-control" type="text" name="search">
                    <button id="findHouse" type="submit" class="btn-primary" style="align-self: center; margin-left: 10px"> Search </button>
                </form>
            </div>

            <div class="row justify-content-center">

                @foreach($houses as $house)
                
                <div class="card house-card d-flex">
                    <div class="card-block d-flex" style=" flex-direction: column; text-align: center">
                        <label> Titulo: {{ $house->title }} </label>
                        <label> Price: {{ $house->value }} </label>
                        <label> ID: {{ $house->id }} </label>
                    </div>
                    <div class="card-block d-flex" style="justify-content: center">
                        <a href="{{ route('house.show', $house->id)}}" class="btn btn-primary" style="margin-top: 20px;margin-bottom: 20px"> Show </a>
                    </div>
                </div>

                @endforeach
            </div>
        </div>
    </div>
    <div class="d-flex" style="margin-top: 20px; justify-content: center;">
        <?php echo $houses->render(); ?>
    </div>
@endsection