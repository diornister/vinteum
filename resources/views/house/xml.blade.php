@extends('layouts.app')
    
@section('content')    
	<div class="container">
	    <div class="row justify-content-center">
		    <form action="consumeXML" method="POST" enctype="multipart/form-data">
		    	{{ csrf_field() }}
		    	<div class="form-group d-flex" style="flex-direction: column; justify-content: center; margin-top: 20px">
			        <input id="xmlfile" class="form-control" type="file" name="xmlFile" accept="text/xml">
			        <button type="submit" class="btn btn-primary" style="max-width: 150px; align-self: center; margin-top: 20px" name="xml-btn"> Consume XML </button>
			    </div>
		    </form>
		</div>
	</div>
@endsection