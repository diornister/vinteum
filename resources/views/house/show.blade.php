@extends('layouts.app')    

@section('content')    
      <div class="container">
            <div class="col d-flex" style="flex-direction: column;">
                  <div class="row house-box">
                        <label name="title"> <strong>Title:</strong> {{ $house->title }} </label>
                        <label name="type"> <strong>Type:</strong> {{ $house->type }} </label>
                        <label name="value"> <strong>Value:</strong> {{ $houseDesc[0]->value }} </label>
                        <label name="sqrtValue"> <strong>SqrtValue:</strong> {{ $houseDesc[0]->sqrtValue }} </label>
                  </div>
                  <div class="row house-box">
                        <label name="type"> <strong>Type:</strong> {{ $houseDesc[0]->type }} </label>
                        <label name="qttyBed"> <strong>QttyBed:</strong> {{ $houseDesc[0]->qttyBed }} </label>
                        <label name="qttyBath"> <strong>QttyBath:</strong> {{ $houseDesc[0]->qttyBath }} </label>
                        <label name="qttyLiving"> <strong>QttyLiving:</strong> {{ $houseDesc[0]->qttyLiving }} </label>
                        <label name="garage"> <strong>Garage:</strong> {{ $houseDesc[0]->garage }} </label>
                  </div>
                  <div class="row house-box">                        
                        <strong>Description:</strong> <br>
                        <label name="description"> {{ $houseDesc[0]->description }} </label>
                  </div>
                  <div class="row house-box">
                        <label name="cep"> <strong>Cep:</strong> {{ $address[0]->cep }} </label>
                        <label name="city"> <strong>City:</strong> {{ $address[0]->city }} </label>
                        <label name="state"> <strong>State:</strong> {{ $address[0]->state }} </label>
                  </div>
                  <div class="row house-box">
                        <label name="block"> <strong>Block:</strong> {{ $address[0]->block }} </label>
                        <label name="number"> <strong>Number:</strong> {{ $address[0]->number }} </label>
                  </div>
                  <div class="row house-box">
                        <strong>Complement:</strong> <br>
                        <label name="complement"> {{ $address[0]->complement }} </label>
                  </div>
            </div>
            <div id="slide" class="carousel slide" data-ride="carousel">
                  <div class="carousel-inner">
                        @foreach($images as $key => $image)
                              @if($key == 0)
                                    <div class="carousel-item active">
                                          <img class="d-block w-100" src="{{$image->url}}" alt="{{$image->name}}">
                                    </div>
                              @else
                                    <div class="carousel-item">
                                          <img class="d-block w-100" src="{{$image->url}}" alt="{{$image->name}}">
                                    </div>
                              @endif
                        @endforeach
                  </div>
                  <a class="carousel-control-prev" href="#slide" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" style="background-color: black" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                  </a>
                  <a class="carousel-control-next" href="#slide" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" style="background-color: black" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                  </a>
            </div>
      </div>
@endsection