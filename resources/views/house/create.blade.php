@extends('layouts.app')    

@section('content')

    <script>
        $(document).ready(function(){
            $("form").keypress(function(e) {
              //Enter key
              if (e.which == 13) {
                return false;
              }
            });

            $('#fetchCEP').on('click', function(event){
                    event.preventDefault();
                    var cep = document.getElementById('cepNumber').value;
                    cep = cep.replace(/\D/g,'');

                    if(cep.length == 8){                        
                        $.ajax({
                            url: 'https://viacep.com.br/ws/'+cep+'/json',
                            method:"POST",
                            success:function(data)
                            {
                                document.getElementById('city').value = data.localidade;
                                document.getElementById('state').value = data.uf;
                                document.getElementById('complement').value = data.logradouro;
                                document.getElementById('block').value = data.bairro;
                            }
                        });
                    }
            });
        });
    </script>

    <div class="container">     
        <form action="/addHouse" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="row d-flex" style="justify-content: space-around;">            
                <div class="form-group house-form-body">
                    <input class="form-control" type="text" name="title" placeholder="title">
                    <input class="form-control" type="text" name="type" placeholder="type">
                    <input class="form-control" type="text" name="value" placeholder="value">
                    <input class="form-control" type="text" name="sqrtValue" placeholder="sqrtValue">
                </div>
                <div class="form-group house-form-body">
                    <input class="form-control" type="text" name="type" placeholder="type">
                    <input class="form-control" type="text" name="qttyBed" placeholder="qttyBed">
                    <input class="form-control" type="text" name="qttyBath" placeholder="qttyBath">
                    <input class="form-control" type="text" name="qttyLiving" placeholder="qttyLiving">
                    <input class="form-control" type="text" name="garage" placeholder="garage">
                </div>
                <div class="form-group house-form-body">
                    <label><strong>Description: </strong></label> <br>
                    <textarea rows="5" cols="50" class="form-control" type="text" name="description" placeholder="description"></textarea>
                </div>
            </div>
            <div class="row d-flex" style="justify-content: space-around;">
                <div class="form-group house-form-body">
                    <div id="fetchCEP" class="d-flex" style="flex-direction: row; justify-content: space-around;">
                        <input id="cepNumber" class="form-control" type="text" name="cep" placeholder="cep">                        
                        <button id="fetchCEP" type="button" class="btn"> Fetch CEP </button>
                    </div>
                    <input id='city' class="form-control" type="text" name="city" placeholder="city">
                    <input id='state' class="form-control" type="text" name="state" placeholder="state">
                    <input id='block' class="form-control" type="text" name="block" placeholder="block">
                    <input class="form-control" type="text" name="number" placeholder="number">
                </div>
                <div class="form-group house-form-body">
                    <label><strong>Complemento: </strong></label> <br>
                    <textarea id="complement" rows="5" cols="50" class="form-control" type="text" name="complement" placeholder="complement"></textarea>
                </div>                
            </div>
            <div class="d-flex row form-group" style="justify-content: center;">
                <div class="d-flex" style="flex-direction: column; justify-content: center">
                    <input class="form-control" type="file" id="gallery" name="files[]" multiple />
                    <button type="submit" class="btn btn-success" style="margin-top: 10px;" > Salvar </button>
                </div>
            </div>
        </form>
    </div>
@endsection
