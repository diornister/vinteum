@extends('layouts.app')    

@section('content')
      <div class="container">
            <form action="/house/{{$house->id}}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
                  <div class="col d-flex" style="flex-direction: column;">
                        <div class="row house-box">
                              <label>Title </label>
                              <input name="title" value="{{ $house->title }}"/>
                              <label>Type</label>
                              <input name="type" value="{{ $house->type }}"/>
                              <label>Value</label>
                              <input name="value" value="{{ $desc[0]->value }}" />
                              <label>SqrtValue</label>
                              <input name="sqrtValue" value="{{ $desc[0]->sqrtValue }}" />
                        </div>
                        <div class="row house-box">
                              <label>Type</label>
                              <input name="type" value="{{ $desc[0]->type }}"/>
                              <label>QttyBed</label>
                              <input name="qttyBed" value="{{ $desc[0]->qttyBed }}"/>
                              <label>QttyBath</label>
                              <input name="qttyBath" value="{{ $desc[0]->qttyBath }}">
                              <label>QttyLiving</label>
                              <input name="qttyLiving" value="{{ $desc[0]->qttyLiving }}"/>
                              <label>Garage</label>
                              <input name="garage" value="{{ $desc[0]->garage }}" />
                        </div>
                        <div class="row house-box">
                              <label>Description</label><br>
                              <textarea rows="5" cols="50" class="form-control" type="text" name="description"> {{ $desc[0]->description }} </textarea>
                        </div>
                        <div class="row house-box">
                              <label>Cep</label>
                              <input name="cep" value="{{ $address[0]->cep }}"/>
                              <label>City</label>
                              <input name="city" value="{{ $address[0]->city }}"/>
                              <label>State</label>
                              <input name="state" value="{{ $address[0]->state }}" />
                        </div>
                        <div class="row house-box">
                              <label>Block</label>
                              <input name="block" value="{{ $address[0]->block }}"/>
                              <label>Number</label>
                              <input name="number" value="{{ $address[0]->number }}"/>
                        </div>
                        <div class="row house-box">                        
                              <label>Complement</label><br>
                              <textarea rows="5" cols="50" class="form-control" type="text" name="complement" > {{ $address[0]->complement }} </textarea>
                        </div>
                        <div class="row house-box">
                              <button type="submit" class="btn btn-success" style="margin-top: 10px;" > Salvar </button>
                        </div>
                  </div>    
            </form>        
      </div>
      <script>
            
      </script>
@endsection