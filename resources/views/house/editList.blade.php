@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            @foreach($houses as $house)
            
            <div class="card house-card d-flex">
                <div class="card-block d-flex" style=" flex-direction: column; text-align: center">
                    <label> ID: {{ $house->id }} </label>
                    <label> Titulo: {{ $house->title }} </label>
                </div>
                <div class="card-block d-flex" style="justify-content: space-around;">
                    <a href="{{ route('house.edit', $house->id)}}" class="btn btn-warning" style="margin-top: 20px;margin-bottom: 20px"> Edit </a>                    
                    <form method="POST" action="{{ route('house.destroy', $house->id)}}">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}

                        <input type="submit" class="btn btn-danger" style="margin-top: 20px;margin-bottom: 20px" value="Delete">
                    </form>
                </div>
            </div>

            @endforeach
        </div>
    </div>
    <div class="d-flex" style="margin-top: 20px; justify-content: center;">
        <?php echo $houses->render(); ?>
    </div>
@endsection
