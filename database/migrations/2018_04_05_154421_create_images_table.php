<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::enableForeignKeyConstraints();

        Schema::create('images', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100);
            $table->integer('house_id')->unsigned();
            $table->string('url');
            $table->timestamps();
            
        });

        Schema::table('images', function(Blueprint $table){
            $table->foreign('house_id')
              ->references('id')->on('houses')
              ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('images');
    }
}
