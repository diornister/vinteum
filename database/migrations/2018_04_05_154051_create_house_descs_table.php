<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHouseDescsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::enableForeignKeyConstraints();

        Schema::create('house_descs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('house_id')->unsigned();
            $table->float('value');
            $table->float('sqrtValue');
            $table->string('type');
            $table->integer('qttyBed');
            $table->integer('qttyBath');
            $table->integer('qttyLiving');
            $table->integer('garage');
            $table->string('description', 1000);
            $table->timestamps();
        });

        Schema::table('house_descs', function(Blueprint $table){
            $table->foreign('house_id')
              ->references('id')->on('houses')
              ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('house_descs');
    }
}
