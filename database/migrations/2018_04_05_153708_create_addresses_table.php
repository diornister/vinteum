<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::enableForeignKeyConstraints();

        Schema::create('addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('house_id')->unsigned();
            $table->string('cep', 9); 
            $table->string('city'); 
            $table->string('state'); 
            $table->string('block'); 
            $table->string('number'); 
            $table->string('complement', 1000);
            $table->timestamps();
            
        });

        Schema::table('addresses', function(Blueprint $table){
            $table->foreign('house_id')
              ->references('id')->on('houses')
              ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
