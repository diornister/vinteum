<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use illuminate\pagination;
use Illuminate\Support\Facades\Input;
use Auth;
use App\House;

use App\Http\Controllers\HouseDescController as House_Desc;
use App\Http\Controllers\AddressController as House_Address;
use App\Http\Controllers\ImageController as House_Image;

class HouseController extends Controller
{

    public function __construct(){

        $this->HouseDesc = new House_Desc();
        $this->Address = new House_Address();
        $this->Image = new House_Image();

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $houses = House::leftJoin('house_descs', 'houses.id', '=', 'house_descs.house_id')
            ->select('houses.*', 'house_descs.*')
            ->paginate(6);
        return view('house.view')->with(compact('houses'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function editIndex()
    {
        $user = Auth::user();
        $houses = House::where('user_id', '=', $user->id )
            ->paginate(6);
        return view('house.editList')->with(compact('houses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('house.create');
    }

    public function validateRequest(Request $request)
    {
        $this->validate($request, array(                
            'title' => 'required',
            'type' => 'required'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $this->validateRequest($request);

        // create House        
        $house = House::create([
            'title' => $request->title,
            'type' => $request->type,
            'user_id' => $user->id
        ]);

        $house = $house->fresh();

        $this->HouseDesc->store($request, $house->id);        
        $this->Address->store($request, $house->id);        
        
        if(sizeof($request->files) > 0){
            var_dump('first working');
            $this->Image->upload($request, $house->id);            
        }

        // redirect to the list page

         return redirect()->route('houseList');
    }

    private function formatDataSet($imovel)
    {
        $result = [];
        $photos = [];

        $result['title'] = (string)$imovel->TipoImovel;
        $result['type'] = (string)$imovel->SubTipoImovel;
        $result['cep'] = (string)$imovel->CEP;
        $result['city'] = (string)$imovel->Cidade;
        $result['state'] = (string)$imovel->UF;
        $result['block'] = (string)$imovel->Bairro;
        $result['number'] = (int)$imovel->Numero;
        $result['complement'] = (string)$imovel->Complemento;
        $result['value'] = ((float)$imovel->PrecoVenda !== '') ? (float)$imovel->PrecoVenda  : ((float)$imovel->PrecoLocacao !== '')? (float)$imovel->PrecoLocacao : (float)$imovel->PrecoLocacaoTemporada ;
        $area = ((float)$imovel->AreaTotal > 0) ? (float)$imovel->AreaTotal : 1;
        $result['sqrtValue'] = ($result['value']/sqrt($area));
        $result['type'] = 'Casa';
        $result['qttyBed'] = (int)$imovel->QtdDormitorios + (int)$imovel->QtdSuites;;
        $result['qttyBath'] = (int)$imovel->QtdBanheiros;
        $result['qttyLiving'] = (int)$imovel->QtdSalas;
        $result['garage'] = (int)$imovel->QtdVagas;
        $result['description'] = (string)$imovel->Observacao;
        
        foreach ($imovel->Fotos as $foto) {            
            array_push($photos, $foto);
        }
        $result['photos'] = $photos;
        return $result;
    }

    private function storeXML($result)
    {
        $user = Auth::user(); 
         
        $house = House::create([
            'title' => (string)$result['title'],
            'type' => (string)$result['type'],
            'user_id' => $user->id
        ]);

        $house = $house->fresh();

        $this->HouseDesc->storeXML($result, $house->id);
        $this->Address->storeXML($result, $house->id);
        $this->Image->uploadXML($result, $house->id);        
    }

    public function consumeXML(Request $request)
    {        
        $file = $request->file('xmlFile');                
        
        if($file){

            $content = utf8_encode(file_get_contents($file));
            $xml = simplexml_load_string($content);            

            foreach ($xml->Imoveis->Imovel as $value) {                
                $result = $this->formatDataSet($value);                
                $this->storeXML($result);
            }
        }

        return view('home');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {        
        $house = House::find($id);
        $houseDesc = $this->HouseDesc->find($id);
        $address = $this->Address->find($id);
        $images = $this->Image->find($id);

        return view('house.show')->with(compact('house'))->with(compact('houseDesc'))->with(compact('address'))->with(compact('images'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $house = House::find($id);
        $desc = $this->HouseDesc->find($house->id);
        $address = $this->Address->find($house->id);
        
        return view('house.edit')
            ->with(compact('house'))
            ->with(compact('desc'))
            ->with(compact('address'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $house = House::findOrFail($id);
        $user = Auth::user();

        if($house->user_id == $user->id){
            $house->title = $request->title;
            $house->type = $request->type;
            $house->save();

            $this->HouseDesc->update($request, $id);
            $this->Address->update($request, $id);
        }else{
            return view('house.view');
        }

        return view('house.edit');
    }

    public function search(){
        $searchedId = Input::post('search');
        
        $house = House::find($searchedId);

        if($house){
            return redirect()->route('house.show', $house->id);
        }else{
            return redirect()->route('houseList');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        House::destroy($id);

        return redirect()->route('editHouse');
    }
}
