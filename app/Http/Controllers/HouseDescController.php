<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HouseDesc;

class HouseDescController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $houseId
     */
    public function store(Request $request, $houseId)
    {
        $houseDesc = new HouseDesc();
        
        $houseDesc->value = $request->value;
        $houseDesc->sqrtValue = $request->sqrtValue;
        $houseDesc->type = $request->type;
        $houseDesc->qttyBed = $request->qttyBed;
        $houseDesc->qttyBath = $request->qttyBath;
        $houseDesc->qttyLiving = $request->qttyLiving;
        $houseDesc->garage = $request->garage;
        $houseDesc->description = $request->description;
        $houseDesc->house_id = $houseId;

        $houseDesc->save();

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $houseId
     */
    public function storeXML($request, $houseId)
    {
        $houseDesc = new HouseDesc();
        
        $houseDesc->value = $request['value'];
        $houseDesc->sqrtValue = $request['sqrtValue'];
        $houseDesc->type = $request['type'];
        $houseDesc->qttyBed = $request['qttyBed'];
        $houseDesc->qttyBath = $request['qttyBath'];
        $houseDesc->qttyLiving = $request['qttyLiving'];
        $houseDesc->garage = $request['garage'];
        $houseDesc->description = $request['description'];
        $houseDesc->house_id = $houseId;

        $houseDesc->save();

    }

    public function find($id){
        $desc = HouseDesc::where('house_id', '=', $id)->get();        
        return $desc;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $desc = HouseDesc::where('house_id', '=', $id)->get();

        $desc->value = $request->value;
        $desc->sqrtValue = $request->sqrtValue;
        $desc->type = $request->type;
        $desc->qttyBed = $request->qttyBed;
        $desc->qttyBath = $request->qttyBath;
        $desc->qttyLiving = $request->qttyLiving;
        $desc->garage = $request->garage;
        $desc->description = $request->description;

        $desc->save();
    }
}
