<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Image;

class ImageController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param int $houseId
     */
    public function upload(Request $request, $houseId )
    {        
        var_dump("working");
        $publicPath = public_path();
        $files = $request->file('files');
        
        foreach($files as $file){        
            $image = new Image();

            $fileName = time().'.'.$file->getClientOriginalExtension();
            $folderpath  = public_path('images');
            $file->move($folderpath , $fileName);

            $image->name = $fileName;
            $image->url = $folderpath.$fileName;
            $image->house_id = $houseId;

            $image->save();
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return int $houseId
     */
    public function uploadXML($request, $houseId )
    {        
        $publicPath = public_path();
        
        foreach($request['photos'][0] as $file){
            $image = new Image();
            $imageName = (string)$file->NomeArquivo;

            $image->name = str_replace(" ", "", $imageName);
            $image->url = (string)$file->URLArquivo;
            $image->house_id = $houseId;

            $image->save();
        }

    }

    public function find($id){
        $images = Image::where('house_id', '=', $id)->get();        
        return $images;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

}
