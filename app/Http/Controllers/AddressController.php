<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Address;

class AddressController extends Controller
{    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Integer $houseId
     */
    public function store(Request $request, $houseId)
    {
        $address = new Address();

        $address->cep = $request->cep;
        $address->city = $request->city;
        $address->state = $request->state;
        $address->block = $request->block;
        $address->number = $request->number;
        $address->complement = $request->complement;
        $address->house_id = $houseId;
        
        $address->save();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Array $result
     * @param Integer $houseId
     */
    public function storeXML($result, $houseId)
    {
        $address = new Address();

        $address->cep = $result['cep'];
        $address->city = $result['city'];
        $address->state = $result['state'];
        $address->block = $result['block'];
        $address->number = $result['number'];
        $address->complement = $result['complement'];
        $address->house_id = $houseId;
        
        $address->save();
    }

    public function find($id){
        $address = Address::where('house_id', '=', $id)->get();        
        return $address;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $address = Address::where('house_id', '=', $id)->get(); 

        $address->cep = $request->cep;
        $address->city = $request->city;
        $address->state = $request->state;
        $address->block = $request->block;
        $address->number = $request->number;
        $address->complement = $request->complement;

        $address->save();
    }
}
