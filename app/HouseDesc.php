<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HouseDesc extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'value', 'sqrtValue', 'type', 'qttyBed', 'qttyBath', 'qttyLiving', 'garage', 'description',
    ];
}
